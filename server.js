const path = require('path');
const express = require('express');
const http = require('http');
const socketio = require('socket.io');
const formatMessages = require('./utils/messages');
const { 
	userJoin,
	getCurrentUser,
	userLeave,
	getRoomUsers
} = require('./utils/users');

const app = express();
const server = http.createServer(app);
const io = socketio(server);

const PORT = 3300 || process.env.PORT;
const botName = 'CharCord Bot';

app.use(express.static(path.join(__dirname, 'public')));

// Run when client connects
io.on('connection', socket => {
	// join to chat room
	socket.on('joinRoom', ({username, room}) => {
		const user = userJoin(socket.id, username, room);

		socket.join(user.room);

		// Welcome to current user
		socket.emit('message', formatMessages(botName, 'Welcome to chatcord'));

		// Broadcast when user connect chat
		socket.broadcast.to(user.room).emit(
			'message', 
			formatMessages(botName, `${user.username} has joinned the chat`)
		);

		// Send users and room info
		io.to(user.room).emit('roomUsers', {
			room: user.room,
			users: getRoomUsers(user.room)
		});
	});

	// Listen for chatMessage
	socket.on('chatMessage', msg => {
		const user = getCurrentUser(socket.id);
	
		io.to(user.room).emit('message', formatMessages(user.username, msg));
	});

	// Run when client disconnect
	socket.on('disconnect', () => {
		const user = userLeave(socket.id);

		if (user) {
			io.to(user.room).emit(
				'message', 
				formatMessages(botName, `${user.username} has left chat`)
			);	

			// Send users and room info
			io.to(user.room).emit('roomUsers', {
				room: user.room,
				users: getRoomUsers(user.room)
			});			
		}
		
	});	
})

server.listen(PORT, function () {
  console.log(`CORS-enabled web server listening on port ${PORT}`)
});


